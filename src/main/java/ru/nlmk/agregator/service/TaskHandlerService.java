package ru.nlmk.agregator.service;

public interface TaskHandlerService {

    long handleBrand(String brand);

    void handleModel(Long brandId, String name);

}
