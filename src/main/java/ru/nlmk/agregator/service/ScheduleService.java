package ru.nlmk.agregator.service;

import java.util.concurrent.TimeUnit;

public interface ScheduleService {

    void scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit);

}
