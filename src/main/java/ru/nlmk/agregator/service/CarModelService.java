package ru.nlmk.agregator.service;

public interface CarModelService {

    long saveModel(long brandId, String model);

}
