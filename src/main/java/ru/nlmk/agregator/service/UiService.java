package ru.nlmk.agregator.service;

import ru.nlmk.agregator.model.Node;

import java.util.List;

public interface UiService {

    List<Node> getRoot();

    List<Node> getById(Long id);

}
