package ru.nlmk.agregator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.agregator.model.CarBrand;
import ru.nlmk.agregator.repository.CarBrandRepository;
import ru.nlmk.agregator.service.CarBrandService;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CarBrandServiceImpl implements CarBrandService {

    private CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandServiceImpl(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    @Override
    @Transactional
    public long saveBrand(String brand) {
        Optional<CarBrand> carBrand = carBrandRepository.findByName(brand);
        if (!carBrand.isPresent()){
            CarBrand newCarBrand = new CarBrand();
            newCarBrand.setName(brand);
            carBrandRepository.save(newCarBrand);
            carBrand = carBrandRepository.findByName(brand);
        }
        return carBrand.get().getId();
    }

}
