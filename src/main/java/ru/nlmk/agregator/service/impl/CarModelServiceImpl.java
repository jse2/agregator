package ru.nlmk.agregator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.agregator.model.CarBrand;
import ru.nlmk.agregator.model.CarModel;
import ru.nlmk.agregator.repository.CarBrandRepository;
import ru.nlmk.agregator.repository.CarModelRepository;
import ru.nlmk.agregator.service.CarModelService;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CarModelServiceImpl implements CarModelService {

    private final CarBrandRepository carBrandRepository;

    private final CarModelRepository carModelRepository;

    @Autowired
    public CarModelServiceImpl(CarBrandRepository carBrandRepository, CarModelRepository carModelRepository) {
        this.carBrandRepository = carBrandRepository;
        this.carModelRepository = carModelRepository;
    }

    @Override
    @Transactional
    public long saveModel(long brandId, String model) {
        Optional<CarModel> carModel = carModelRepository.findByBrandIdAndName(brandId, model);
        if (!carModel.isPresent()){
            CarBrand newCarBrand = carBrandRepository.getById(brandId);
            CarModel newCarModel = new CarModel();
            newCarModel.setName(model);
            newCarModel.setBrandId(newCarBrand.getId());
            carModelRepository.save(newCarModel);
            carModel = carModelRepository.findByBrandIdAndName(brandId, model);
        }
        return carModel.get().getId();
    }

}
