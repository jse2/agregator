package ru.nlmk.agregator.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nlmk.agregator.config.AppConfig;
import ru.nlmk.agregator.service.CarBrandService;
import ru.nlmk.agregator.service.CarModelService;
import ru.nlmk.agregator.service.TaskHandlerService;

import java.util.concurrent.*;

@Slf4j
@Component
public class TaskHandlerServiceImpl implements TaskHandlerService {

    private final ExecutorService executorService;

    private final CarBrandService carBrandService;

    private final CarModelService carModelService;

    @Autowired
    public TaskHandlerServiceImpl(AppConfig appConfig, CarBrandService carBrandService, CarModelService carModelService) {
        this.carBrandService = carBrandService;
        this.carModelService = carModelService;
        int countHandlers = appConfig.getTaskHandlers();
        executorService = new ThreadPoolExecutor(countHandlers,
                countHandlers,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    @Override
    public long handleBrand(String brand) {
        return carBrandService.saveBrand(brand);
    }

    @Override
    public void handleModel(Long brandId, String name) {
        executorService.submit(() -> {
            carModelService.saveModel(brandId, name);
        });
    }

}
