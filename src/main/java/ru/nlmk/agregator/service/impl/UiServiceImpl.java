package ru.nlmk.agregator.service.impl;

import org.springframework.stereotype.Service;
import ru.nlmk.agregator.model.Node;
import ru.nlmk.agregator.repository.CarBrandRepository;
import ru.nlmk.agregator.repository.CarModelRepository;
import ru.nlmk.agregator.service.UiService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UiServiceImpl implements UiService {

    private final CarBrandRepository carBrandRepository;

    private final CarModelRepository carModelRepository;

    public UiServiceImpl(CarBrandRepository carBrandRepository, CarModelRepository carModelRepository) {
        this.carBrandRepository = carBrandRepository;
        this.carModelRepository = carModelRepository;
    }

    @Override
    public List<Node> getRoot() {
        return carBrandRepository.findAll()
                .stream()
                .map(brand -> new Node(brand.getId().toString(), "#", brand.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Node> getById(Long id) {
        return carModelRepository.findByBrandId(id)
                .stream()
                .map(model -> new Node(model.getBrandId().toString() + model.getId().toString(), model.getBrandId().toString(), model.getName()))
                .collect(Collectors.toList());
    }

}
