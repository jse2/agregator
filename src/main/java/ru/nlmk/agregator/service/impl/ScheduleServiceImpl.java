package ru.nlmk.agregator.service.impl;

import ru.nlmk.agregator.service.ScheduleService;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduledThreadPoolExecutor pool;

    public ScheduleServiceImpl(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException();
        }
        pool = new ScheduledThreadPoolExecutor(size);
    }

    public void scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        pool.scheduleAtFixedRate(command, initialDelay, period, unit);
    }

}
