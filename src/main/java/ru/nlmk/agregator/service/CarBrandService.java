package ru.nlmk.agregator.service;

import ru.nlmk.agregator.model.CarBrand;

import java.util.List;

public interface CarBrandService {

    long saveBrand(String brand);

}
