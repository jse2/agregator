package ru.nlmk.agregator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    private DictionaryConfig dictionary;

    private int clientPool;

    private int taskHandlers;

    @Data
    public static class DictionaryConfig {

        private List<String> urls;

        private String brandsPath;

        private String modelsPath;

        private long maxAttempts;

        private long minBackoffMs;

        private long commonTimeoutSec;

    }

}
