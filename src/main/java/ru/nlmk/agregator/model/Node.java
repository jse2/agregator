package ru.nlmk.agregator.model;

import lombok.Data;

@Data
public class Node {

    private String id;

    private String parent;

    private String text;

    private boolean children;

    public Node(String id, String parent, String text) {
        this.id = id;
        this.parent = parent;
        this.text = text;
        this.children = true;
    }

}
