package ru.nlmk.agregator.worker;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import ru.nlmk.agregator.client.ClientProxy;
import ru.nlmk.agregator.client.impl.ClientProxyImpl;
import ru.nlmk.agregator.config.AppConfig;
import ru.nlmk.agregator.service.TaskHandlerService;

@Slf4j
public class CatalogClient implements Runnable {

    private final ClientProxy proxy;

    private final String url;

    private final TaskHandlerService taskHandlerService;

    public CatalogClient(String url, AppConfig appConfig, TaskHandlerService taskHandlerService) {
        this.url = url;
        this.proxy = new ClientProxyImpl(appConfig, WebClient.create(url));
        this.taskHandlerService = taskHandlerService;
    }

    @Override
    public void run() {
        log.info("Start aggregation task for url {}", url);
        proxy.getBrands()
                .map(brand -> {
                    log.info("Brand id {} and name {}", brand.getId(), brand.getName());
                    long idSaved = taskHandlerService.handleBrand(brand.getName());
                    proxy.getModels(brand.getId())
                            .map(model -> {
                                log.info("Model id {} and name {}", model.getId(), model.getName());
                                taskHandlerService.handleModel(idSaved, model.getName());
                                return model;
                            }).subscribe();
                    return brand;
                }).subscribe();
    }

}
