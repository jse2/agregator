package ru.nlmk.agregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmk.agregator.model.CarBrand;
import java.util.Optional;

@Repository
public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {

    Optional<CarBrand> findByName(String name);

}
