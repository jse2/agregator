package ru.nlmk.agregator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmk.agregator.model.CarModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel, Long> {

    Optional<CarModel> findByBrandIdAndName(long brandId, String name);

    List<CarModel> findByBrandId(long brandId);

}
