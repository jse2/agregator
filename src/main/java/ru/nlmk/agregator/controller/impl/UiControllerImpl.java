package ru.nlmk.agregator.controller.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.agregator.controller.UiController;
import ru.nlmk.agregator.model.Node;
import ru.nlmk.agregator.service.UiService;
import java.util.List;

@RestController
public class UiControllerImpl implements UiController {

    private final UiService uiService;

    public UiControllerImpl(UiService uiService) {
        this.uiService = uiService;
    }

    @Override
    @RequestMapping("/ui")
    public ResponseEntity<List<Node>> getNodes(@RequestParam String id) {
        if (id == null) {
            return ResponseEntity.badRequest().build();
        } else {
            List<Node> nodes;
            if ("root".equals(id)) {
                nodes = uiService.getRoot();
            } else {
                try {
                    nodes = uiService.getById(Long.parseLong(id));
                } catch (Exception exception) {
                    return ResponseEntity.badRequest().build();
                }
            }
            return ResponseEntity.ok(nodes);
        }
    }

}
