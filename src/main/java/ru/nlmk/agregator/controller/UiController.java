package ru.nlmk.agregator.controller;

import org.springframework.http.ResponseEntity;
import ru.nlmk.agregator.model.Node;

import java.util.List;

public interface UiController {

    ResponseEntity<List<Node>> getNodes(String id);

}
