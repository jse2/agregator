package ru.nlmk.agregator.client;

import reactor.core.publisher.Flux;
import ru.nlmk.agregator.dto.CarBrandDTO;
import ru.nlmk.agregator.dto.CarModelDTO;

public interface ClientProxy {

    Flux<CarBrandDTO> getBrands();

    Flux<CarModelDTO> getModels(Long id);

}
