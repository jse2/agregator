package ru.nlmk.agregator.client.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import reactor.core.publisher.Flux;
import ru.nlmk.agregator.client.ClientProxy;
import ru.nlmk.agregator.config.AppConfig;
import ru.nlmk.agregator.dto.CarBrandDTO;
import ru.nlmk.agregator.dto.CarModelDTO;

import java.time.Duration;

import static java.time.Duration.ofMillis;
import static reactor.util.retry.Retry.backoff;


@Slf4j
public class ClientProxyImpl implements ClientProxy {

    private static final String CONNECTION_REFUSED_ERROR_MSG = "Connection refused";

    private final WebClient client;

    private final AppConfig appConfig;

    public ClientProxyImpl(AppConfig appConfig, WebClient client) {
        this.appConfig = appConfig;
        this.client = client;
    }

    @Override
    public Flux<CarBrandDTO> getBrands() {
        var cfg = appConfig.getDictionary();
        return client.get()
                .uri(cfg.getBrandsPath())
                .exchangeToFlux(response -> {
                    if (response.statusCode().equals(HttpStatus.OK)) {
                        return response.bodyToFlux(CarBrandDTO.class);
                    } else {
                        return Flux.empty();
                    }
                }).retryWhen(backoff(cfg.getMaxAttempts(), ofMillis(cfg.getMinBackoffMs()))
                        .filter(this::isRequestRetryException))
                .timeout(Duration.ofSeconds(cfg.getCommonTimeoutSec()))
                .onErrorResume(throwable -> Flux.empty());
    }

    @Override
    public Flux<CarModelDTO> getModels(Long id) {
        var cfg = appConfig.getDictionary();
        return client.get()
                .uri(cfg.getModelsPath() + id)
                .exchangeToFlux(response -> {
                    if (response.statusCode().equals(HttpStatus.OK)) {
                        return response.bodyToFlux(CarModelDTO.class);
                    } else {
                        return Flux.empty();
                    }
                }).retryWhen(backoff(cfg.getMaxAttempts(), ofMillis(cfg.getMinBackoffMs()))
                        .filter(this::isRequestRetryException))
                .timeout(Duration.ofSeconds(cfg.getCommonTimeoutSec()))
                .onErrorResume(throwable -> Flux.empty());
    }

    private boolean isRequestRetryException(final Throwable throwable) {
        return throwable instanceof WebClientRequestException
                && StringUtils.hasLength(throwable.getMessage())
                && throwable.getMessage().startsWith(CONNECTION_REFUSED_ERROR_MSG);
    }

}
