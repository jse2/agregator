package ru.nlmk.agregator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.nlmk.agregator.config.AppConfig;
import ru.nlmk.agregator.service.ScheduleService;
import ru.nlmk.agregator.service.TaskHandlerService;
import ru.nlmk.agregator.service.impl.ScheduleServiceImpl;
import ru.nlmk.agregator.worker.CatalogClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class App {

    private final AppConfig appConfig;

    private ScheduleService scheduleService;

    private final TaskHandlerService taskHandlerService;

    @Autowired
    public App(AppConfig appConfig, TaskHandlerService taskHandlerService) {
        this.appConfig = appConfig;
        this.taskHandlerService = taskHandlerService;
    }

    /**
     * Main loop of program
     */
    @EventListener(ContextRefreshedEvent.class)
    public void run() {
        List<String> urls = appConfig.getDictionary().getUrls();
        if (urls == null || urls.size() == 0) {
            throw new IllegalArgumentException();
        }
        scheduleService = new ScheduleServiceImpl(appConfig.getClientPool());
        long timeout = appConfig.getDictionary().getCommonTimeoutSec();
        long initDelay = 0;
        long period = timeout * urls.size();
        for (int i = 0; i < urls.size(); ++i) {
            String url = urls.get(i);
            Runnable task = new CatalogClient(url, appConfig, taskHandlerService);
            scheduleService.scheduleAtFixedRate(task, initDelay, period, TimeUnit.SECONDS);
            initDelay += timeout;
        }
    }

}
