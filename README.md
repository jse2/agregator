# 1. Информация о проекте

Агрегатор справочников автомобилей

# 2. Системные требования

1. Java 11
2. PostgreSQL

# 3. Сборка проекта
```cmd
mvn spring-boot:build-image
```

# 4. Настроечные параметры
Находятся в application.yml.
*  app.dictionary.urls - список опрашиваемых адресов
*  app.dictionary.client-pool - размер пула потоков для запроса данных
*  app.dictionary.task-handlers - размер пула потоков для обработки результатов запросов